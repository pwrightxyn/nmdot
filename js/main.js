'use strict';
// IIFE - Immediately Invoked Function Expression
(function(init) {

	// The global jQuery object is passed as a parameter
	init(window.jQuery, window, document);
	
	}(function($, window, document) {
		
		var $primaryNavButton = $('#PrimaryNavButton');
		var $megaMenu = $('#MegaMenu');
		var $subnav = $('.subnav');
		var menuOpen = false;
	
		function toggleMenu(){
			menuOpen = !menuOpen;
			$('header').toggleClass('active');
			$megaMenu.toggleClass('hidden active');
			$primaryNavButton.toggleClass('active');
			$primaryNavButton.children('img').toggleClass('active hidden');
			$('#SearchOverlay').addClass('hidden');
			if (menuOpen){
				$(document).keyup(function(ev) {
				     if (ev.keyCode == 27 && menuOpen) { 
					    // escape key maps to keycode `27`
				        toggleMenu();
				    }
				});
			}
		}
		$primaryNavButton.on('click', function(){
			toggleMenu();
		});
		
		$('.navTitle i').on('click keyup', function(ev){
			if (ev.type === "keyup" && ( ev.keyCode !== 13 && ev.keyCode !== 32)){
				return;
			}
			var $thisParent = $(this.parentNode.parentNode);
			var $thisSubnav = $thisParent.children('.subnav');
			var $thisTitle = $(this.parentNode);
			if ($thisSubnav){
				ev.preventDefault();
				$thisTitle.toggleClass('open');
				$thisSubnav.toggleClass('open');
				$thisParent.toggleClass('open');
			} else{
					
			}
		});
		
		$('#MobileDropdownToggle').on('click keyup', function(ev){
			if (ev.type === "keyup" && ( ev.keyCode !== 13 && ev.keyCode !== 32)){
				return;
			}
			var $sidebar = $('.sidebar');
			$sidebar.toggleClass('active');
			
		});	
		
		$('.contactMain').on('click keyup', function(ev){
			if (ev.type === "keyup" && ( ev.keyCode !== 13 && ev.keyCode !== 32)){
				return;
			}
			var $contactEntry = $(this.parentNode);
			var $contactDetails = $contactEntry.children('.contactDetails');
			var $contactPlusMinus =$contactEntry.children('.contactMain').children('i');
			$contactPlusMinus.toggleClass('fa-plus fa-minus');
			$contactEntry.toggleClass('active');
			
		});
		
		$('.directoryTab').on('click keyup', function(ev){
			if (ev.type === "keyup" && ( ev.keyCode !== 13 && ev.keyCode !== 32)){
				return;
			}
			var $thisTab = $(this);
			var $theseTabs = $(this.parentNode).children('.directoryTab');
			var thisTarget = $thisTab.data('directorytab');
			
			// toggle active state on tab
			$theseTabs.toggleClass('active');
			$theseTabs.find('.fa-plus, .fa-minus').toggleClass('fa-plus fa-minus');
			
			// show target content
			$('.directoryContent').toggleClass('active');
		});
		
		$('.contentTitle').on('click keyup',
			function(ev){
				if (ev.type === "keyup" && ( ev.keyCode !== 13 && ev.keyCode !== 32)){
				return;
			}
				$(this.parentNode).toggleClass('active');
				$(this).children('.fa-plus, .fa-minus').toggleClass('fa-plus fa-minus'); //flip the plus to minus and vice versa
			}
		);
		
		$('.searchTxt').on('focus', function(ev){
			$('#SearchOverlay').removeClass('hidden');
		});
		$('#SearchOverlay').on('click keyup', function(ev){
			if (ev.type === "keyup" && ( ev.keyCode !== 13 && ev.keyCode !== 32)){
				return;
			}
			$('#SearchOverlay').addClass('hidden');
		});
		
		$('#CarouselContainer .linksColumn a').on('mouseenter mouseleave', function(ev){
			$(this).find('.hover').toggleClass('active');		
		});
	}
));